# How to add local pdf extension


### Steps (just follow all):

#### 1. Download bolt

`
composer create-project bolt/composer-install:^3.5 ./ --prefer-dist --no-interaction
`
#### 2. Config database in `app/config.yml`

#### 3. Run server
`
php app/nut server:run
`
#### 4. Open localhost and create first root user

#### 5. Create folder `local` in `extensions`

#### 6. Copy all files `tools` in to `extensions/local`

#### 7. In terminal `cd extensions`

#### 8. Copy and past this two lines
`composer config repositories.tools-pdf path local/tools/pdf`

`composer require tools/pdf`

#### 9. Add to the `public/theme/base-2018/partials/_record_meta.twig`

```php
{% if record|pdf %}
        <a href="{{ absolute_url(record|pdf) }}" class="button is-link" target="_blank">Download PDF</a>
{%  endif %}
```
#### 10. Add pages or entries and try how it works!

#### 11. You can find all pdfs in `/public/files/pdf/[typ]/[id].pdf`

![](https://preview.ibb.co/eJoVad/Zrzut_ekranu_2018_05_10_o_13_12_31.png)

&copy; Paweł Wiewióra  {pawel.wiewiora@mnslab.pl} {pawel.wiewiora@hotmail.com}

