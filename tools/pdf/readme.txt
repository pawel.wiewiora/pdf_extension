Generator PDF
Wtyczka generuje PDF'y do postów zaraz po ich zapisaniu.
PDFy znajdują się w folderze: /public/files/pdf/[typ]/[id].pdf

INSTALACJA:

1. Skopiuj folder: tools do /extenstions/local/

2. Odpal konsole i przejdź do /extensions i następnie wykonaj 2 polecenia:
    - composer config repositories.tools-pdf path local/tools/pdf
    - composer require tools/pdf

3. Do swojego szablonu dodaj (np.w  pliku: _record_meta.twig)
    {% if record|pdf %}
        <a href="{{ absolute_url(record|pdf) }}" class="button is-link" target="_blank">PDF</a>
     {%  endif %}
