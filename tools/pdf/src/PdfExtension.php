<?php

namespace Bolt\Extension\Tools\Pdf;

use Bolt\Extension\SimpleExtension;
use Bolt\Events\StorageEvents;
use Bolt\Events\StorageEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Silex\Application;

/**
 * Pdf extension class.
 */
class PdfExtension extends SimpleExtension
{
    private $contentType = ['entries', 'pages']; //typy postów które mają być obsługiwane
    private $fs;
    /**
     * {@inheritdoc}
     */
    public function registerServices(Application $app)
    {
        $manager = $app['filesystem'];
        $this->fs = $manager->getFilesystem('files');
    }

    /**
     * Rejestracja zdarzeń które mają być nasłuchiwane
     * @param EventDispatcherInterface $dispatcher
     */
    protected function subscribe(EventDispatcherInterface $dispatcher)
    {
        $dispatcher->addListener(StorageEvents::POST_SAVE, [$this, 'postSaveCallback']);
    }

    /**
     * Moteda wywoływana po zapisaniu danych
     * @param StorageEvent $event
     */
    public function postSaveCallback(StorageEvent $event)
    {
        if (in_array($event->getContentType(), $this->contentType)) {
            $data = $event->getContent()->getValues();
            $data['id'] = $event->getId();
            $data['type'] = $event->getContentType();
            $data['link'] = $event->getContent()->link();
            $data['image'] = !is_null($data['image'])?(array_key_exists('file',$data['image'])?$data['image']['file']:null):null;

            $this->generatePdf($data);
        }
    }

    /**
     * Rejestracja nowego filtru Twig
     * @return array
     */
    protected function registerTwigFilters()
    {
        return [
            'pdf' => 'pdfFilter'
        ];
    }

    /**
     * Generowanie filtru Twig który służy do tworzenia linku do PDF
     * przykład _record_meta.twig:
     *
     * {% if record|pdf %}
     *      <a href="{{ absolute_url(record|pdf) }}" class="button is-link" target="_blank">PDF</a>
     * {%  endif %}
     *
     *
     * @param $record
     * @return bool|string
     */
    public function pdfFilter($record)
    {
        if ($record && isset($record->values) && isset($record->contenttype) && in_array(strtolower($record->contenttype['name']), $this->contentType)) {
            if (file_exists(realpath('public/files') . '/pdf/' . strtolower($record->contenttype['name']) . '/' . $record->id . '.pdf')) {
                return '/files/pdf/' . strtolower($record->contenttype['name']) . '/' . $record->id . '.pdf';
            }
        }
        return false;
    }

    /**
     * Generowanie PDF
     * @param $data - ['id','type','title','body','image']
     */
    public function generatePdf($data)
    {
        if (is_array($data) && !in_array(['id', 'type', 'title', 'body', 'image'], $data)) {

            if(!$this->fs->has('pdf/'.$data['type'])) $this->fs->createDir('pdf/'.$data['type']);

            $dirObj = $this->fs->getDir('/pdf/' . $data['type']);

            require_once __DIR__ . '/libs/tcpdf/tcpdf.php';
            $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->addPage();
            $pdf->writeHTML('<h1 style="text-align: center;">' . $data['title'] . '</h1>');
            if(!is_null($data['image'])) $pdf->writeHTML('<img src="public/files/'. $data['image'].'"/>');
            $pdf->writeHtml('<p>' . $data['body'] . '</p>');
            $pdf->Output(realpath('public/files') . '/pdf/' . $data['type'] . '/' . $data['id'] . '.pdf', 'F');
        }
    }
}